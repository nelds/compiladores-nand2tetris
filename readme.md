# Capítulo 1. Compiladores, interpretadores e máquinas virtuais

# 1 Por que estudar compiladores ?

Segundo Cooper Keith,

> "Compiladores são programas grandes e complexos, e geralmente incluem centenas de milhares, ou mesmo milhões, de linhas de código, organizadas em múltiplos subsistemas e componentes. As várias partes de um compilador interagem de maneira complexa. Decisões de projeto tomadas para uma parte do compilador têm ramificações importantes para as outras. Assim, o projeto e a implementação de um compilador é um exercício substancial em engenharia de software".
> 

O autor ainda destaca que um bom compilador contém um microcosmo da ciência da computação, incluindo:

- algoritmos gulosos (alocação de registradores), técnicas de busca heurística (agendamento de lista), algoritmos de grafos (eliminação de código morto),
- programação dinâmica (seleção de instruções), autômatos finitos e autômatos de pilha (análises léxica e sintática) e algoritmos de ponto fixo (análise de fluxo de dados).
- Lida com problemas, como alocação dinâmica, sincronização, nomeação, localidade, gerenciamento da hierarquia de memória e escalonamento de pipeline.

No projeto Nand2Tetris, que vamos usar aqui os autores destacam os seguintes conceitos trabalhados:

- Virtual machine
- Stack architecture
- OO implementation
- Unit testing
- Memory ManagementCompiladores
- Linked list
- Graphical output
- Pointers
- Symbol table
- Grammars
- Two-tier compilation
- Code generation
- Sprites

# 2 Compiladores

É comum visualizar um compilador com três fases principais:

![pictures/cap1/Untitled.png](pictures/cap1/Untitled.png)

Fonte: Cooper Keith

Podemos, visualizar em um pouco mais de detalhes cada uma destas caixas como abaixo:

![pictures/cap1/Untitled%201.png](pictures/cap1/Untitled%201.png)

## 3 Representação e linguagens intermediárias

Onde IR (Intermediate Representation), é a representação intermediaria que é o conjunto de estruturas de dados para representar o código que é processado. A vantagem da utilização de uma representação intermediária é facilmente entendida na seguinte figura:

![pictures/cap1/Untitled%202.png](pictures/cap1/Untitled%202.png)

Fonte: [https://www.cs.princeton.edu/courses/archive/spr03/cs320/notes/IR-trans1.pdf](https://www.cs.princeton.edu/courses/archive/spr03/cs320/notes/IR-trans1.pdf)

A utilização de compiladores em três estágios é empregado no projeto LLVM. Talvez tenham ouvido falar do compilador Clang, que é usado pela Apple e no [repl.it](http://repl.it).  Algumas linguagens mais recentes tem usado essa plataforma, como Rust e Swift.

![pictures/cap1/Untitled%203.png](pictures/cap1/Untitled%203.png)

Um exemplo de código na LLVM IR:

![pictures/cap1/Untitled%204.png](pictures/cap1/Untitled%204.png)

Fonte: [https://llvm.org/docs/LangRef.html](https://llvm.org/docs/LangRef.html)

### 3.1 Representações gráficas e ASTs

Uma árvore sintática (parse tree) é uma representação gráfica para a derivação, ou análise sintática, que corresponde ao programa de entrada (Cooper). Essas árvores terão os elementos presentes na sintaxe daquela linguagem, incluindo: parenteses, palavras chaves, demarcadores ..

![Untitled](pictures/cap1/Untitled%205.png)

A árvore sintática abstrata (AST — Abstract Syntax Tree) retém a estrutura essencial da árvore de derivação, mas elimina os nós irrelevantes.  A precedência e o significado da expressão permanecem, mas os nós irrelevantes desapareceram (Cooper).  Por exemplo,  para AST não importa se um bloco é demarcado por chaves, endentação ou palavras reservadas como `begin` e `end`. Por exemplo, uma AST escrita em go para um comando de atribuição:

![Untitled](pictures/cap1/Untitled%206.png)

Fonte: Thosten Ball, Writing an interpreter.

No site abaixo é possível visualizar ASTs para diferentes linguagens:

[AST explorer](https://astexplorer.net/)

### 3.2 Representações Lineares

IRs lineares são semelhantes ao pseudocódigo para alguma máquina abstrata.Os algoritmos percorrem sequências de operações lineares, simples.  Como destacado por Cooper Keith, a lógica por trás do uso do formato linear é simples. O código-fonte que serve como entrada para o compilador está em um formato linear, assim como o código da máquina-alvo que ele emite.

Cooper Keith, destaca três tipos de linguagens lineares, baseado no número de argumentos:

Códigos de três endereços

Códigos de três endereços modelam uma máquina na qual a maioria
das operações utiliza dois operandos e produzem um resultado. O surgimento de arquiteturas RISC nas décadas de 1980 e 1990 tornaram esses códigos populares, pois são semelhantes a uma máquina RISC simples.

O nome “três endereços” está associado à especificação, em uma instrução que representa uma operação binária, de no máximo três variáveis: duas para os operadores e uma para o resultado. Assim, expressões complexas envolvendo diversas operações devem ser decompostas nessa linguagem em uma série de
instruções elementares (Ricarte).

Considerando o uso de rotulos para indicar uma posição no programa, ricarte apresenta um exemplo de um mapeamento de um comando while para uma representação em três endereços.

```java
while (i++ <= k) {
	x[i] = 0;
}
x[0] = 0;
```

Para o seguinte comando

![Untitled](pictures/cap1/Untitled%207.png)

Códigos de dois endereços

Códigos de dois endereços modelam uma máquina com operações destrutivas. Esses códigos caíram em desuso quando as restrições de memória se tornaram menos importantes; um código de três endereços pode modelar operações destrutivas explicitamente.

Códigos de um endereço

Códigos de um endereço modelam o comportamento das máquinas acumuladoras e de pilha. Esses códigos expõem o uso de nomes implícitos, de modo que o compilador pode ajustar o código a este uso. O código resultante é bastante compacto.

Códigos baseados em pilha são exemplos de códigos de um endereço, que assume a presença de uma pilha de operandos.

Por exemplo, um pseudo-código para uma máquina de pilha para a expressão a-2 * b

```cpp
push
push b
multiply
push a
subtract
```

Este código é compacto. A pilha cria um espaço de nomes implícito e elimina muitos nomes da IR, encurtando o tamanho de um programa no formato IR. 

Porém, o uso da pilha significa que todos os resultados e argumentos são transitórios, a menos que o código os mova explicitamente para a memória.

Código de máquina de pilha é simples de gerar e de executar. Smalltalk 80 e Java utilizam bytecodes, uma IR semelhante em conceito ao código de máquina de pilha.

Mesmo linguagens interpretadas, normalmente geram e executam bytecodes. O seguinte codigo em Python:

```python
def f(a,b):
  return a-2*b
```

gera o seguinte bytecode:

```cpp
5           0 LOAD_FAST                0 (a)
            2 LOAD_CONST               1 (2)
            4 LOAD_FAST                1 (b)
            6 BINARY_MULTIPLY
            8 BINARY_SUBTRACT
            10 RETURN_VALUE
```

Mais recente, outra linguagem baseada em stack que tem chamado atenção é a web assembly: [https://webassembly.org/](https://webassembly.org/)mais recente, outra linguagem baseada em stack que tem chamado atenção é a web assembly: [https://webassembly.org/](https://webassembly.org/)mais recente, outra linguagem baseada em stack que tem chamado atenção é a web assembly: [https://webassembly.org/](https://webassembly.org/)

Máquinas virtuais como a JVM utilizam o IR baseadas em pilhas e fixam que cada instrução tem que ser representada por um byte, por isso, bytecode. Além disso, armazenam o código gerado em um arquivo. Separando a compilação da execução.

<aside>
👉🏼 Máquinas virtuais são interpretadores que executam bytecode.

</aside>

<aside>
👉🏼 Saber mais sobre o bytecode do Java veja em [https://dzone.com/articles/introduction-to-java-bytecode](https://dzone.com/articles/introduction-to-java-bytecode)

</aside>

No site abaixo podemos visualizar a representação intermediária de algumas linguagens:

[Compiler Explorer](https://godbolt.org/)

# 4. Interpretadores e máquinas virtuais

Um interpretador pode ser basicamente uma rotina que percorre uma árvore AST e para cada tipo de nó aplica uma ação:

```java
@Override
    public Void visitWhileStmt(Stmt.While stmt) {
        while (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.body);
        }
        return null;
    }
```

A figura abaixo representa esse processo.

![pictures/cap1/Untitled%208.png](pictures/cap1/Untitled%208.png)

Uma AST pode ser usada ainda para produzir o código na linguagem simbólica ou em uma linguagem linear. Onde essa linguagem poderia ser interpretada, como na figura a seguir.

![pictures/cap1/Untitled%209.png](pictures/cap1/Untitled%209.png)

Ou mesmo, para ser usado para gera um código nativo, como descrito anteriormente

![Untitled](pictures/cap1/Untitled%2010.png)

## 5. Macros, syntatic sugar e  transpiladores

As [s-expressions](https://en.wikipedia.org/wiki/S-expression) usadas em linguagens da família lisp são basicamente representações de uma AST. Essas linguagens são poderosas por essa característica, que facilita o desenvolvimento de macros. Que basicamente, são transformações da AST. Mesmo linguagens que tem uma sintaxe especifica como Elixir. Escrever um programa em Lisp é como escrevermos diretamente a AST que é avaliada pelo interpretador. Quando não queremos que o interpretador avalie, temos que usar o quote, que em Lisp é o simbolo ', e em elixir é a palavra quote.

O seguinte comando em Elixir

```java
quote do: 2 * 3 + 1
```

Retorna a seguinte AST:

```java
{:+, _, [{:*, _, [2, 3]}, 1]}
```

Que se trocarmos os demarcadores por parenteses, teriamos algo como:

```java
(+ (* 2 3) 1)
```

Macros então são transformações que são realizadas nas AST.  Isso permitiu por exemplo usar lisp como uma linguagem de programação orientada a objeto sem ter que alterar o interpretador.

Transformações de AST podem ser usadas para criar syntatic sugar para alguns recursos em uma linguagem sem impactar no interpretador ou gerador de código.

E por fim, alguns transpiladores podem são basicamente transformações em ASTs. Transpilador é um termo comumente usado para referir a tradução entre linguagens de alto nível, como JavaScript e TypeScript. Uma abordagem possível é incluir um novo componente que devolve uma nova AST:

![pictures/cap1/Untitled%2011.png](pictures/cap1/Untitled%2011.png)

# 6. Nosso compilador

Iremos usar os ferramentais e a metodologia proposa no projeto [Nand2Tetris](https://www.nand2tetris.org/):

![pictures/cap1/Untitled%2012.png](pictures/cap1/Untitled%2012.png)

O compilador será dividido em dois projetos:

Compilador

![pictures/cap1/Untitled%2013.png](pictures/cap1/Untitled%2013.png)

Tradutor para assembly

![pictures/cap1/Untitled%2014.png](pictures/cap1/Untitled%2014.png)

Nesse curso, iremos implementar um compilador:

- em duas etapas, dado que não iremos usar um otimizador
- análise léxica poderá ser implementada manualmente, ou com auxilio de expressões regulares.
- iremos usar um analisador sintático preditivo implementado manualmente
- não será gerado uma AST, iremos gerar diretamente uma representação intermediária (stack-based)
- no fim, essa linguagem intermediária será traduzida para um código Assembly de uma arquitetura de computador simplificada